module gitlab.com/golang-demos/blog-api

go 1.13

require (
	github.com/go-kit/kit v0.10.0
	github.com/gocql/gocql v0.0.0-20200228163523-cd4b606dd2fb
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/prometheus/client_golang v1.3.0
	github.com/sirupsen/logrus v1.4.2
)
